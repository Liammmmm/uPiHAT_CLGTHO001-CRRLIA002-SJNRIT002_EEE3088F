EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7450 7500 0    50   ~ 0
Power Supply: 3V3, 12V
Text Notes 8150 7650 0    50   ~ 0
04/06/2021
$Comp
L Transistor_BJT:2N3055 Q1
U 1 1 60B9F789
P 5250 3650
F 0 "Q1" V 5578 3650 50  0000 C CNN
F 1 "2N3055" V 5487 3650 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-3" H 5450 3575 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N3055-D.PDF" H 5250 3650 50  0001 L CNN
	1    5250 3650
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Linear:LT1085-3.3 U1
U 1 1 60BA150C
P 6400 3550
F 0 "U1" H 6400 3792 50  0000 C CNN
F 1 "LT1085-3.3" H 6400 3701 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6400 3800 50  0001 C CIN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/1083ffe.pdf" H 6400 3550 50  0001 C CNN
	1    6400 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 60BA444D
P 5800 3800
F 0 "C1" H 5918 3846 50  0000 L CNN
F 1 "1u" H 5918 3755 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 5838 3650 50  0001 C CNN
F 3 "~" H 5800 3800 50  0001 C CNN
	1    5800 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 60BA6D04
P 7000 3800
F 0 "C2" H 7118 3846 50  0000 L CNN
F 1 "10u" H 7118 3755 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 7038 3650 50  0001 C CNN
F 3 "~" H 7000 3800 50  0001 C CNN
	1    7000 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R1
U 1 1 60BA8DB2
P 4650 3800
F 0 "R1" H 4582 3754 50  0000 R CNN
F 1 "27" H 4582 3845 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 4690 3790 50  0001 C CNN
F 3 "~" H 4650 3800 50  0001 C CNN
	1    4650 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 3550 5800 3550
Wire Wire Line
	5800 3650 5800 3550
Connection ~ 5800 3550
Wire Wire Line
	5800 3550 6100 3550
$Comp
L power:GND #PWR01
U 1 1 60BAA799
P 5250 4650
F 0 "#PWR01" H 5250 4400 50  0001 C CNN
F 1 "GND" H 5255 4477 50  0000 C CNN
F 2 "" H 5250 4650 50  0001 C CNN
F 3 "" H 5250 4650 50  0001 C CNN
	1    5250 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 60BAB553
P 6400 4100
F 0 "#PWR02" H 6400 3850 50  0001 C CNN
F 1 "GND" H 6405 3927 50  0000 C CNN
F 2 "" H 6400 4100 50  0001 C CNN
F 3 "" H 6400 4100 50  0001 C CNN
	1    6400 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4450 5250 4650
Wire Wire Line
	4650 3950 5250 3950
Wire Wire Line
	5250 3850 5250 3950
Connection ~ 5250 3950
Wire Wire Line
	5250 3950 5250 4150
Wire Wire Line
	4650 3650 4650 3550
Wire Wire Line
	4650 3550 5050 3550
Wire Wire Line
	5800 3950 5800 4000
Wire Wire Line
	5800 4000 6400 4000
Wire Wire Line
	7000 4000 7000 3950
Wire Wire Line
	6400 3850 6400 4000
Connection ~ 6400 4000
Wire Wire Line
	6400 4000 7000 4000
Wire Wire Line
	6400 4100 6400 4000
Wire Wire Line
	6700 3550 7000 3550
Wire Wire Line
	7000 3550 7000 3650
Wire Wire Line
	7000 3550 7400 3550
Connection ~ 7000 3550
Wire Wire Line
	5800 3200 5800 3550
Wire Wire Line
	5800 3200 7400 3200
Text GLabel 7400 3550 2    50   Input ~ 0
3V3_Vcc
Text GLabel 7400 3200 2    50   Input ~ 0
12V_Vcc
Wire Wire Line
	4650 3550 4050 3550
Connection ~ 4650 3550
Text GLabel 4050 3550 0    50   Input ~ 0
Power_Input
$Comp
L Device:D_Zener D1
U 1 1 60BA2379
P 5250 4300
F 0 "D1" V 5204 4380 50  0000 L CNN
F 1 "BZX85C12" V 5295 4380 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 5250 4300 50  0001 C CNN
F 3 "~" H 5250 4300 50  0001 C CNN
	1    5250 4300
	0    1    1    0   
$EndComp
$EndSCHEMATC
